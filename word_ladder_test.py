import unittest
import word_ladder


class Test_Word_Ladder(unittest.TestCase):


    def test_load_file(self):
        self.assertEqual(word_ladder.load_file("test"), "The file name you have entered cannot be found \n")



    def test_validate_start_word(self):
        self.assertEqual(word_ladder.validate_start_word("mean"), "mean")



    def test_validate_target_word(self):
        self.assertEqual(word_ladder.validate_target_word("hide"), "hide")



    def test_select_path(self):
        self.assertEqual(word_ladder.select_path("s"), 's')


    def test_build_forbidden_words_list(self):
        result = word_ladder.build_forbidden_words_list("head bead mead")
        self.assertTrue(type(result) is list)


    def test_same(self):
        self.assertTrue(word_ladder.same("ate", "are") == 2)
        self.assertEqual(word_ladder.same("eat", "ate"), 0)
        self.assertEqual(word_ladder.same("test", "testing"), 4)
        self.assertEqual(word_ladder.same("1", "1"), 1)
        self.assertRaises(TypeError, word_ladder.same("test", "find"), 1)



    def test_build(self):
        self.assertFalse(word_ladder.build(".ead", "bead, mead, dead, head, lead", {"lead": True}, []) == False)


    def test_find(self):
        self.assertTrue(word_ladder.find("lead", "lead, load, goad, gold", {"lead" : True}, "gold", ["lead"]) == False )





if __name__ == '__main__':
    unittest.main()