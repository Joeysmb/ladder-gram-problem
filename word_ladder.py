import re

#  function which prompts user for dictionary name and handles FileNotFound exceptions.
def file_reader():
    while True:
        try:
            fname = input("Please enter the dictionary name: ")
            return load_file(fname)
        except FileNotFoundError:
            print("The file name you have entered cannot be found!\n")

def load_file(file):
    return open(file, "r")

#function which prompts the user for a start word input and re-prompts until the input is satisfactory
def validate_start_word(word):
    while True:
        if word.isdigit():
            word = input("Word must not be a number. Choose another start word\n")
        elif len(word) < 2:
            word = input("Word must be at least 2 letters long. Choose another start word\n")
        elif word.isalpha():
            word.replace(" ", "")  # removes every space in a typed word
            return word

#same as start word function except it also ensures that the target word word the user inputs is the same length as the start word.
def validate_target_word(word):
    global start
    while True:
        if word.isdigit():
            word = input("Word must not be a number. Choose another start word\n")
        elif len(word) != len(start):
            word = input("Word must be the same length as the start word you choose before. Choose another target word\n")
        elif word.isalpha():
            word.replace(" ", "")  # removes every space in a typed word
            return word

#  function which builds the list of the words which the user wishes to exclude from the search function and sanitizes the input to ensure the program will run correctly.
def build_forbidden_words_list(words):
    forbidden_words = words.split(" ")
    return forbidden_words

def select_path(path):
    path.lower()
    while True:
        if (path.isdigit()):
            path = input("Path cannot be a number").lower()
        elif len(path) > 1:
            path = input("Select either L or S \n").lower()
        else:
            if path == 'l':
                return 'l'
            elif path == 's':
                return 's'
            else:
                path = input("Select either L or S \n").lower()
                continue

def same(item, target):
  return len([c for (c, t) in zip(item, target) if c == t])

def build(pattern, words, seen, list):
  return [word for word in words
                 if re.search(pattern, word) and word not in seen.keys() and
                    word not in list]



def find(word, words, seen, target, path):
    # decides which path to  take either the longest path or the shortest path
    # if 'l' take the  longest path
    # if 's take the shortest path
    global path_selected
    # list of  alphabet with the least occurrence in all the words in the dictionary.
    global rare_alphabets
    # it contains the list of words that contain the word in the pattern and exist in the dictionary and also not in seen
    list = []

    # generate a pattern from the start word.
    for i in range(len(word)):  # for loop which iterates through the word's letters based on the length of the chosen word.
        list += build(word[:i] + "." + word[i + 1:], words, seen, list)  # uses '.' as a wild card
    # if the length of the list is zero return false

    if len(list) == 0:
        return False
    # take short path(if 's' is selected)
    if path_selected == 's':
        # (same(w, target), w) returns a tuple showing the (count_match,item)
        # it returns the number of times the alphabet in the item(W)  matches with the alphabet in the target word respectively
        # the ordering is done in descending order
        list = sorted([(same(w, target), w) for w in list], reverse=True)
    else:
        # take long path(if 'L' is selected)
        rare_alphabets = []
        # (same(w, target), w) returns a tuple showing the (count_match,item)
        # it returns the number of times the alphabet item(W)  matches with the alphabet in the target word respectively
        # the ordering is done in ascending order
        list = sorted([(same(w, target), w) for w in list])
    for (match, item) in list:  # iterating through the match and word pairs in list
        for letter in rare_alphabets:
            if letter in item:
                # if the item word contains any of the rare alphabet, remove the word from the list
                list.remove((match, item))
        if match >= len(target) - 1:
            # if match is greater than or equal to the length  of the target word ,then target word is found
            if match == len(target) - 1:
                path.append(item)
            # return true if target word is found
            return True
        # list showing item already seen to avoid repetition
        seen[item] = True
    for (match, item) in list:
        path.append(item)
        if find(item, words, seen, target, path):
            # if  target word is found return true
            return True
        # removes the item added to the list if the  item produces empty list .
        # if function find() returns  false.it shows that the word pattern generated from the item does not have a word
        # in the dictionary that contains the pattern thereby making the list empty.And when the list is empty,
        # it returns false and removes the item from the list because the word is not valid.
        path.pop()


#running all of the functions of the program
#main function starts here
file = file_reader()
lines = file.readlines()


while True:
    # while loop which gathers all of the appropriate user input required for the program to run successfully.
    path = input("Do you want the shortest path or the longest path? Please input 'L' for the longest path or 'S' for the shortet path: \n")
    path_selected = select_path(path.lower())
    word = input("Please enter start word:")
    start = validate_start_word(word)
    word = input("Please enter target word:")
    target = validate_target_word(word)
    #user input to accept the forbidden wors
    words_list = str(input("Enter space seperated words that you want to be excluded from the laddergram.\n Example: mind boom \n")).replace(" ", "")
    forbidden_words = build_forbidden_words_list(words_list)
    words = []
    for line in lines:
        word = line.rstrip()
        #  removes the forbidden words from the dictionary
        if len(word) == len(start) and word not in forbidden_words:
            words.append(word)
    break

# rare  alphabets with the least occurrence
rare_alphabets = ["q", "x", "y", "z"]
#list of valid paths to the target
path = [start]
#dictionary of word seen
seen = {start : True}
if find(start, words, seen, target, path):
  path.append(target)
  print(len(path) - 1, path)
else:
  print("No path found")
